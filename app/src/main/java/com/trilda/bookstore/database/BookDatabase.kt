package com.trilda.bookstore.database

import androidx.room.Database
import androidx.room.RoomDatabase

import com.trilda.bookstore.Book

const val DATABASE_NAME = "book_store_db"

@Database(entities =[Book::class], version = 1)
abstract class BookDatabase : RoomDatabase() {
    abstract fun bookDao() : BookDao
}
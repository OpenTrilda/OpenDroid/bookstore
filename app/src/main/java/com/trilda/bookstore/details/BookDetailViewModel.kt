package com.trilda.bookstore.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.trilda.bookstore.App
import com.trilda.bookstore.Book

class BookDetailViewModel(bookId: Int) : ViewModel()  {

    private val bookIdLiveData = MutableLiveData<Int>()

    var book: LiveData<Book> = Transformations.switchMap(bookIdLiveData) { id ->
        App.db.bookDao().getBookById(id)
    }

    init {
        bookIdLiveData.value = bookId
    }
}